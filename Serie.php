<?php

class Serie{

    protected $nombre;
    protected $temporada = [];
    protected $capitulos = [];

    function __construct($nombre, $temporada, $capitulos)
    {
        $this->nombre = $nombre;
        $this->temporada = $temporada;
        $this->capitulos = $capitulos;
    }
}