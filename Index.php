<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>

<body>
    <form action="main.php" method="POST">
        <div class="main">
            <div class="cabezera">
                <h1>Agrega Contenido</h1>
            </div>
            <div class="formulario">
                <div class="serie">
                    <p>Serie</p>
                    <select name="serie" id="">
                        <option value="">El marginal</option>
                    </select>
                </div>
                <div class="temporada">
                    <p>Temporada</p>
                    <select name="temporada" id="">
                        <option value="temp1">Temporada 1</option>
                        <option value="temp2">Temporada 2</option>
                        <option value="temp3">Temporada 3</option>
                    </select>
                </div>
                <div class="capitulo">
                    <p>Nombre del capitulo</p>
                    <input type="text" name="capitulo" id="" placeholder="Nombre">
                </div>
            </div>
            <div class="btns">
                <input type="button" value="Enviar">
            </div>
        </div>
    </form>

</body>

</html>